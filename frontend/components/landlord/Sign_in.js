import React from "react";
import Link from "next/link";
import Router from "next/router";
import hasLogo from "../../public/static/HAS.png";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { CURRENT_USER_QUERY } from "../queryComponents/User";
import {
  Container,
  Input,
  PrimaryButton,
  ActionButton,
  SubHeader
} from "../styledComponents";
import loadingImg from "../../public/static/126.gif";

const LOGIN_MUTATION = gql`
  mutation LOGIN_MUTATION($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      user_name
      email
    }
  }
`;

class Sign_in extends React.Component {
  state = {
    email: "",
    password: ""
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    const { email, password } = this.state;
    return (
      <Container flex={1} column>
        <Container middle center>
          <img src={hasLogo} style={{ width: "7%", height: "7%" }} />
        </Container>
        <Container
          middle
          center
          flex={1}
          column
          customStyles={{ minHeight: "50vh" }}
        >
          <Container middle center>
            <SubHeader>Please Login</SubHeader>
          </Container>
          <Mutation
            mutation={LOGIN_MUTATION}
            variables={this.state}
            refetchQueries={[{ query: CURRENT_USER_QUERY }]}
          >
            {(login, { error, loading }) => {
              return (
                <form
                  method="post"
                  onSubmit={async e => {
                    e.preventDefault();
                    const res = await login();
                    if (res) {
                      Router.replace("/landlord/dashboard");
                    }
                    this.setState({ email: "", password: "" });
                  }}
                >
                  <fieldset
                    disabled={loading}
                    aria-busy={loading}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      border: "none",
                      width: 200
                    }}
                  >
                    {error &&
                      error.graphQLErrors.map(({ message }, i) => (
                        <p key={i} style={{ color: "red" }}>
                          {message}
                        </p>
                      ))}

                    <Container flex={1} middle center>
                      <Input
                        id="email"
                        label="E-mail"
                        type="email"
                        name="email"
                        placeholder="enter email"
                        style={{ marginBottom: 10, width: 200 }}
                        value={email}
                        onChange={this.handleChange}
                      />
                    </Container>

                    <Container middle center>
                      <Input
                        id="password"
                        label="Password"
                        type="password"
                        name="password"
                        placeholder="enter password"
                        style={{ marginBottom: 10, width: 200 }}
                        value={password}
                        onChange={this.handleChange}
                      />
                    </Container>
                  </fieldset>
                  <Container
                    space="space-between"
                    row
                    center
                    customStyles={{ paddingLeft: 10, paddingRight: 10 }}
                  >
                    <Link href="/landlord/signup">
                      <a>
                        <ActionButton>Create account</ActionButton>
                      </a>
                    </Link>
                    <Container>
                      <PrimaryButton type="submit">
                        {loading ? (
                          <img
                            src={loadingImg}
                            style={{ width: 20, height: 20 }}
                            alt="loading..."
                          />
                        ) : (
                          "Login"
                        )}
                      </PrimaryButton>
                    </Container>
                  </Container>
                </form>
              );
            }}
          </Mutation>
        </Container>
      </Container>
    );
  }
}
export default Sign_in;
