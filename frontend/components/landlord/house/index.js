import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Link from "next/link";
import {
  Typography,
  Paper,
  InputBase,
  Divider,
  IconButton,
  GridList,
  Avatar,
  Button,
  GridListTile
} from "@material-ui/core";
import { theme as customTheme } from "../../Page";
import CheckIcon from "@material-ui/icons/CheckCircleOutline";
import BedIcon from "@material-ui/icons/SingleBed";
import BathroomIcon from "@material-ui/icons/Bathtub";
import SearchIcon from "@material-ui/icons/Search";
import CustomDrawer from "../Drawer";
import CreateNewHouse from "./create";
import Houses from "../../queryComponents/Houses";
import {
  Container,
  PrimaryButton,
  CommandButton,
  SubText,
  CaptionText,
  NormalText
} from "../../styledComponents";
import DeleteHouse from "./DeleteHouse";
import loadingImg from "../../../public/static/126.gif";
import UpdateHouse from "./UpdateHouse";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flex: 1,
    height: 500,
    flexDirection: "column",
    padding: theme.spacing(1)
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    marginBottom: theme.spacing(2)
    // padding: theme.spacing(0)
    // ...theme.mixins.toolbar
  },
  searchRoot: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 300
  },
  input: {
    marginLeft: 10,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  },
  dividerLine: {
    width: "100%"
    // marginTop: 3
  },
  gridList: {
    display: "flex",
    flex: 1,
    // width: "100%",
    height: "100%",
    overflowY: "hidden"
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)"
  },
  houseTileStyle: {
    margin: 5,
    borderRadius: 2,
    padding: 0,
    backgroundColor: "white",
    boxShadow: `0 3.2px 7.2px 0 rgba(0, 0, 0, 0.132),
    0 0.6px 1.8px 0 rgba(0, 0, 0, 0.108)`,
    "&:hover": {
      // zIndex: 1,
      // backgroundColor: "#0B1B21",
      transform: `scale(1.005) translate(0.2px, 0px)`
    }
  },
  tileWrapper: {
    width: "100%",
    height: "100%"
    // backgroundColor: "pink"
  },
  imageWrapper: {
    // display: "flex",
    // flex: 1,
    // width: 150,
    padding: 2,
    borderRadius: 4,
    transition: `transform 600ms`
    // "&:hover": {
    //   border: "1px solid red"
    // }
  },
  imageStyle: {
    width: "100%",
    height: 90,
    objectFit: "fit"
  }
}));

const Index = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [openSlide, setOpenSlide] = React.useState(false);
  const [house, setHouse] = React.useState({});

  const handleSlideOpen = () => {
    setOpenSlide(true);
  };
  const handleSlideClose = () => {
    setOpenSlide(false);
  };
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const onViewHouse = house => {
    setHouse(house);
    handleSlideOpen();
  };

  const _tileCard = house => {
    const { countryId, provinceId, districtId, images } = house;
    const image = images[0];
    return (
      <Link href={"house/[id]"} as={"house/" + house.id}>
        <a style={{ textDecoration: "none" }}>
          <Container
            flex={1}
            column
            depth={64}
            className={classes.tileWrapper}
            color="white"
          >
            <Container
              middle
              center
              color="transparent"
              customStyles={{
                position: "absolute",
                top: 3,
                right: 3
              }}
            >
              {house.isActive && (
                <CheckIcon style={{ fontSize: 20, color: "green" }} />
              )}
            </Container>
            {images.length === 0 && (
              <Container
                flex={0.6}
                middle
                center
                className={classes.imageWrapper}
                color="transparent"
              >
                <Avatar>{house.house_heading.charAt(0)}</Avatar>
              </Container>
            )}
            {images.length !== 0 && (
              <Container
                flex={0.6}
                className={classes.imageWrapper}
                color="transparent"
              >
                <img src={image.image} className={classes.imageStyle} />
              </Container>
            )}

            <Container
              flex={0.4}
              column
              color="transparent"
              customStyles={{ paddingLeft: 5, paddingRight: 5 }}
            >
              <Typography variant="h6" gutterBottom>
                {house.house_heading}
              </Typography>
              <Container row center flex={0} color="transparent">
                <Typography variant="body2" gutterBottom>
                  {countryId.name + ", "}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  {provinceId.name + ", "}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  {districtId.name}
                </Typography>
              </Container>
              {/* <Divider
                className={classes.dividerLine}
                orientation="horizontal"
             />*/}
              <Container
                row
                center
                flex={0}
                space="space-between"
                color="transparent"
              >
                <Container row center middle flex={1} color="transparent">
                  <BedIcon style={{ fontSize: 18 }} />
                  <span>4</span>
                </Container>
                <Container row center middle flex={1} color="transparent">
                  <BathroomIcon style={{ fontSize: 15 }} />
                  <span>4</span>
                </Container>
                <Container
                  row
                  middle
                  center
                  flex={1}
                  center
                  color="transparent"
                >
                  <Button color="primary" onClick={() => onViewHouse(house)}>
                    Edit
                  </Button>
                </Container>
              </Container>
            </Container>
          </Container>
        </a>
      </Link>
    );
  };
  const _houseCard = house => {
    const { countryId, provinceId, districtId, images } = house;
    const image = images[0];
    return (
      <Container
        flex={1}
        depth={64}
        column
        color="white"
        customStyles={{ width: "100%", height: "100%", position: "relative" }}
      >
        {images.length === 0 && (
          <Container
            flex={0.5}
            middle
            center
            // className={classes.imageWrapper}
            color="transparent"
          >
            <Avatar>{house.house_heading.charAt(0)}</Avatar>
          </Container>
        )}
        {images.length !== 0 && (
          <Container
            flex={0.5}
            // className={classes.imageWrapper}
            color="transparent"
          >
            {/* <img src={image.image} className={classes.imageStyle} />*/}
          </Container>
        )}
        <Container
          flex={0.35}
          column
          customStyles={{ paddingTop: 5, paddingLeft: 5, paddingRight: 5 }}
        >
          <Container
            column
            space="space-around"
            flex={0.6}
            customStyles={{ paddingTop: 5 }}
          >
            <CaptionText customStyles={{ fontWeight: 600 }}>
              {house.house_heading}
            </CaptionText>
            <NormalText customStyles={{ color: "#3A3A3A" }}>
              {provinceId.name + ", " + districtId.name}
            </NormalText>
          </Container>
          <Container flex={0.4} middle>
            <span>heyy</span>
          </Container>
        </Container>
        <Container flex={0}>
          <Divider />
        </Container>

        <Container row flex={0.15}>
          <Container flex={1}>
            <CommandButton
              customStyles={{ height: "100%", fontSize: "12px" }}
              onClick={() => onViewHouse(house)}
            >
              Edit
            </CommandButton>
          </Container>
          <Container>
            <Divider orientation="vertical" />
          </Container>
          <Container flex={1}>
            <CommandButton customStyles={{ height: "100%", fontSize: "12px" }}>
              Delete
            </CommandButton>
          </Container>
        </Container>
      </Container>
    );
  };
  return (
    <Container
      flex={1}
      column
      customStyles={{
        paddingLeft: 20,
        height: "100%"
      }}
    >
      <Container
        flex={1}
        id={"page-container"}
        customStyles={{ position: "relative" }}
      >
        <Container
          id="command-header"
          row
          space="space-between"
          customStyles={{ marginTop: 10, marginLeft: 5, marginBottom: 10 }}
        >
          <Paper component="form" className={classes.searchRoot} elevation={0}>
            <InputBase
              className={classes.input}
              placeholder="Search house by name"
              // inputProps={{ "aria-label": "search google maps" }}
            />

            <Divider className={classes.divider} orientation="vertical" />
            <IconButton
              type="submit"
              className={classes.iconButton}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
          </Paper>

          <Container flex={0.2} row middle center>
            <PrimaryButton onClick={handleDrawerOpen}>
              <span
                style={{
                  marginRight: 5,
                  // marginLeft: 3,
                  padding: 0,
                  fontWeight: 500,
                  fontSize: "24px"
                }}
              >
                +
              </span>
              Add House
            </PrimaryButton>
          </Container>
        </Container>
        <Houses>
          {({ data, loading }) => {
            if (data) {
              const houses = data.getUserHouses;

              if (houses.length !== 0) {
                return (
                  <GridList
                    cellHeight={180}
                    cols={7}
                    className={classes.gridList}
                    justify="center"
                  >
                    {houses.map(house => (
                      <GridListTile
                        key={house.id}
                        className={classes.houseTileStyle}
                      >
                        <Link href={"house/[id]"} as={"house/" + house.id}>
                          <a style={{ textDecoration: "none" }}>
                            {_houseCard(house)}
                          </a>
                        </Link>
                      </GridListTile>
                    ))}
                  </GridList>
                );
              }
              return null;
            }

            return (
              <Container flex={1} column middle center>
                {loading && (
                  <img src={loadingImg} style={{ width: 25, height: 25 }} />
                )}
              </Container>
            );
          }}
        </Houses>
      </Container>

      <CustomDrawer open={openSlide} house={house}>
        <UpdateHouse house={house} onClose={handleSlideClose} />
      </CustomDrawer>
      <CustomDrawer
        onOpen={handleDrawerOpen}
        onClose={handleDrawerClose}
        open={open}
      >
        <CreateNewHouse onClose={handleDrawerClose} />
      </CustomDrawer>
    </Container>
  );
};

export default Index;
