import React from "react";
import { withStyles, fade } from "@material-ui/core/styles";
import { Mutation } from "react-apollo";
import { GET_USER_HOUSES } from "../../../queryComponents/Houses";
import { TextField, FormControl, Select } from "@material-ui/core";
import gql from "graphql-tag";
import { theme as customTheme } from "../../../Page";
import Countries from "../../../queryComponents/Countries";
import Provinces from "../../../queryComponents/Provinces";
import Districts from "../../../queryComponents/Districts";
// import Sectors from "../../queryComponents/Sectors";
import HouseTypes from "../../../queryComponents/HouseTypes";
import {
  Container,
  FlatButton,
  Input,
  Text,
  CaptionText,
  PrimaryButton
} from "../../../styledComponents";
import loadingImg from "../../../../public/static/126.gif";

const CREATE_HOUSE_MUTATION = gql`
  mutation CREATE_HOUSE_MUTATION(
    $house_heading: String!
    $house_description: String!
    $house_no: String!
    $no_bathrooms: Int
    $no_bedrooms: Int
    $no_annex: Int
    $dinning_room: Int
    $isActive: Boolean
    $has_in_kitchen: Boolean
    $has_balcony: Boolean
    $has_garden: Boolean
    $has_hotwater: Boolean
    $has_carparking: Boolean
    $house_type: ID
    # $parent_houseId: ID
    $countryId: ID
    $provinceId: ID
    $districtId: ID # $sectorId: ID
  ) {
    createHouse(
      house_heading: $house_heading
      house_description: $house_description
      house_no: $house_no
      no_bathrooms: $no_bathrooms
      no_bedrooms: $no_bedrooms
      no_annex: $no_annex
      dinning_room: $dinning_room
      isActive: $isActive
      has_in_kitchen: $has_in_kitchen
      has_balcony: $has_balcony
      has_garden: $has_garden
      has_hotwater: $has_hotwater
      has_carparking: $has_carparking
      house_type: $house_type
      # parent_houseId: $parent_houseId
      countryId: $countryId
      provinceId: $provinceId
      districtId: $districtId # sectorId: $sectorId
    ) {
      id
      house_heading
      house_description
      house_no
      no_annex
      no_bathrooms
      no_bedrooms
      dinning_room
      isActive
      has_in_kitchen
      has_balcony
      has_garden
      has_hotwater
      has_carparking
      house_type {
        id
      }
      # parent_houseId {
      #   id
      # }
      countryId {
        id
        name
      }
      provinceId {
        id
        name
      }
      districtId {
        id
        name
      }
      # sectorId {
      #   id
      #   name
      # }
    }
  }
`;

const styles = theme => ({
  list: {
    width: "100%",
    alignItems: "center"
  },
  formControl: {
    marginTop: theme.spacing(2),
    maxWidth: 120
  },
  inputStyle: {
    border: 0,
    borderRadius: 4,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    backgroundColor: "#F5F8FA",
    // lineHeight: 2,
    minHeight: 100,
    fontSize: 12,
    "&:focus": {
      border: "1px solid #4C3AF7",
      backgroundColor: "#FEFEFF"
    }
  },
  button: {
    margin: theme.spacing(1)
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  },
  headerStyle: {
    fontWeight: "500",
    fontSize: 14
  },
  checkStyle: {
    fontWeight: "400",
    fontSize: 12
  }
});

class CreateHouse extends React.Component {
  state = {
    createdHouses: [],
    activeStep: 0,
    house_heading: "",
    house_description: "",
    house_no: "",
    no_annex: "",
    no_bathrooms: "",
    no_bedrooms: "",
    dinning_room: "",
    isActive: false,
    has_in_kitchen: false,
    has_balcony: false,
    has_garden: false,
    has_hotwater: false,
    has_carparking: false,
    house_type: "",
    parent_houseId: "",
    countryId: "",
    provinceId: "",
    districtId: "",
    sectorId: "",
    success: false
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleReset = () => {
    this.setState({ activeStep: 0 });
  };

  render() {
    const {
      activeStep,
      house_heading,
      house_description,
      house_no,
      no_annex,
      no_bathrooms,
      no_bedrooms,
      dinning_room,
      isActive,
      has_in_kitchen,
      has_balcony,
      has_garden,
      has_hotwater,
      has_carparking,
      house_type,
      parent_houseId,
      countryId,
      provinceId,
      districtId,
      sectorId,
      createdHouses,
      success
    } = this.state;
    const { classes } = this.props;

    return (
      <Mutation
        mutation={CREATE_HOUSE_MUTATION}
        refetchQueries={[{ query: GET_USER_HOUSES }]}
        variables={{
          house_heading,
          house_description,
          house_no,
          no_bathrooms: parseInt(no_bathrooms),
          no_bedrooms: parseInt(no_bedrooms),
          no_annex: parseInt(no_annex),
          dinning_room: parseInt(dinning_room),
          isActive,
          has_in_kitchen,
          has_balcony,
          has_garden,
          has_hotwater,
          has_carparking,
          house_type,
          // parent_houseId,
          countryId,
          provinceId,
          districtId
          // sectorId
        }}
      >
        {(createHouse, { loading, error }) => {
          return (
            <Container flex={1}>
              <form
                method="post"
                style={{
                  width: "100%",
                  display: "flex",

                  flexDirection: "column",
                  flex: 1,
                  position: "relative"
                }}
                onSubmit={async e => {
                  e.preventDefault();
                  await createHouse()
                    .then(res => {
                      this.setState({ success: true });
                    })
                    .catch(err => {
                      alert(err);
                    });

                  this.setState({
                    house_heading: "",
                    house_description: "",
                    house_no: "",
                    no_annex: 0,
                    no_bathrooms: 0,
                    no_bedrooms: 0,
                    dinning_room: 0,
                    isActive: false,
                    has_in_kitchen: false,
                    has_balcony: false,
                    has_garden: false,
                    has_hotwater: false,
                    has_carparking: false,
                    house_type: "",
                    parent_houseId: "",
                    countryId: "",
                    provinceId: "",
                    districtId: "",
                    sectorId: ""
                  });
                }}
              >
                <Container
                  flex={1}
                  // color="red"
                  column
                  customStyles={{ overFlowY: "auto", paddingTop: 10 }}
                >
                  {/* house details */}
                  <Container column color="pink">
                    <Container row flex={1} customStyles={{ paddingLeft: 10 }}>
                      <Text customStyles={{ fontWeight: "600" }}>
                        House details
                      </Text>
                    </Container>
                    <Container flex={1} customStyles={{ padding: 10 }} row>
                      <Container
                        column
                        flex={1}
                        customStyles={{ paddingRight: 5 }}
                      >
                        <Input
                          label="House Heading"
                          type="text"
                          value={house_heading}
                          onChange={e => {
                            this.setState({ house_heading: e.target.value });
                          }}
                        />
                        <Input
                          label="House Number"
                          type="text"
                          value={house_no}
                          onChange={e => {
                            this.setState({ house_no: e.target.value });
                          }}
                        />
                        <FormControl
                          size="small"
                          variant="outlined"
                          style={{ marginBottom: 10 }}
                          className={classes.formControl}
                        >
                          <Select
                            native
                            size="small"
                            value={house_type}
                            onChange={e => {
                              this.setState({ house_type: e.target.value });
                            }}
                            inputProps={{
                              name: "age",
                              id: "outlined-age-native-simple"
                            }}
                          >
                            <option value="">type of house</option>
                            <HouseTypes>
                              {({ data }) => {
                                if (data) {
                                  return (
                                    <React.Fragment>
                                      {data.houseTypes.map(type => (
                                        <option key={type.id} value={type.id}>
                                          {type.house_type}
                                        </option>
                                      ))}
                                    </React.Fragment>
                                  );
                                }
                                return null;
                              }}
                            </HouseTypes>
                          </Select>
                        </FormControl>
                      </Container>
                      <Container flex={1} column>
                        <Container
                          column
                          customStyles={{ marginBottom: 10 }}
                          color="transparent"
                        >
                          <CaptionText customStyles={{ marginBottom: 5 }}>
                            Description
                          </CaptionText>
                          <textarea
                            type="text"
                            className={classes.inputStyle}
                            value={house_description}
                            onChange={e => {
                              this.setState({
                                house_description: e.target.value
                              });
                            }}
                          />
                        </Container>
                      </Container>
                    </Container>
                  </Container>

                  {/* house features */}
                  <Container column>
                    <Container row flex={1} customStyles={{ paddingLeft: 10 }}>
                      <Text customStyles={{ fontWeight: "600" }}>
                        House Features
                      </Text>
                    </Container>
                    <Container flex={1} customStyles={{ padding: 10 }} row>
                      <Container column flex={1}>
                        <Container column customStyles={{ paddingRight: 10 }}>
                          <Input
                            label="no. of bedrooms"
                            type="number"
                            value={no_bedrooms}
                            onChange={e => {
                              this.setState({ no_bedrooms: e.target.value });
                            }}
                          />
                          <Input
                            label="no. of bathrooms"
                            type="number"
                            onChange={e => {
                              this.setState({ no_bathrooms: e.target.value });
                            }}
                          />
                        </Container>

                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 10
                          }}
                        >
                          <input
                            type="checkbox"
                            style={{ marginLeft: 10 }}
                            checked={isActive}
                            onChange={e => {
                              this.setState({ isActive: !isActive });
                            }}
                          />
                          <span className={classes.checkStyle}>Has Tenant</span>
                        </Container>
                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 5
                          }}
                        >
                          <input
                            type="checkbox"
                            checked={has_balcony}
                            style={{ marginLeft: 10 }}
                            onChange={e => {
                              this.setState({ has_balcony: !has_balcony });
                            }}
                          />
                          <span className={classes.checkStyle}>
                            has balcony
                          </span>
                        </Container>
                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 5
                          }}
                        >
                          <input
                            type="checkbox"
                            checked={has_in_kitchen}
                            style={{ marginLeft: 10 }}
                            onChange={e => {
                              this.setState({
                                has_in_kitchen: !has_in_kitchen
                              });
                            }}
                          />
                          <span className={classes.checkStyle}>
                            has in kitchen
                          </span>
                        </Container>
                      </Container>
                      <Container column flex={1}>
                        <Container column>
                          <Input
                            label="no. of annex"
                            type="number"
                            value={no_annex}
                            onChange={e => {
                              this.setState({ no_annex: e.target.value });
                            }}
                          />
                          <Input
                            label="dinning rooms"
                            type="number"
                            size="small"
                            value={dinning_room}
                            onChange={e => {
                              this.setState({ dinning_room: e.target.value });
                            }}
                          />
                        </Container>
                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 10
                          }}
                        >
                          <input
                            type="checkbox"
                            checked={has_garden}
                            style={{ marginLeft: 10 }}
                            onChange={e => {
                              this.setState({ has_garden: !has_garden });
                            }}
                          />
                          <span className={classes.checkStyle}>has garden</span>
                        </Container>
                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 5
                          }}
                        >
                          <input
                            type="checkbox"
                            name="check"
                            checked={has_hotwater}
                            style={{ marginLeft: 10 }}
                            onChange={e => {
                              this.setState({ has_hotwater: !has_hotwater });
                            }}
                          />
                          <span className={classes.checkStyle}>
                            has hot water
                          </span>
                        </Container>
                        <Container
                          row
                          center
                          customStyles={{
                            marginTop: 5
                          }}
                        >
                          <input
                            type="checkbox"
                            name="check"
                            checked={has_carparking}
                            style={{ marginLeft: 10 }}
                            onChange={e => {
                              this.setState({
                                has_carparking: !has_carparking
                              });
                            }}
                          />
                          <span className={classes.checkStyle}>
                            has carparking
                          </span>
                        </Container>
                      </Container>
                    </Container>
                  </Container>

                  {/* location forms */}
                  <Container column color="pink">
                    <Container row flex={1} customStyles={{ paddingLeft: 20 }}>
                      <span className={classes.headerStyle}>Location</span>
                    </Container>
                    <Container flex={1} row customStyles={{ padding: 10 }}>
                      <Container flex={1} row>
                        <FormControl
                          size="small"
                          variant="outlined"
                          style={{
                            width: 100,
                            marginRight: 10,
                            marginLeft: 10
                          }}
                          className={classes.formControl}
                        >
                          <Select
                            native
                            size="small"
                            value={countryId}
                            onChange={e => {
                              this.setState({ countryId: e.target.value });
                            }}
                            inputProps={{
                              name: "country",
                              id: "outlined-country-native-simple"
                            }}
                          >
                            <option value="">country</option>
                            <Countries>
                              {({ data }) => {
                                if (data) {
                                  return (
                                    <React.Fragment>
                                      {data.countries.map(country => (
                                        <option
                                          key={country.id}
                                          value={country.id}
                                        >
                                          {country.name}
                                        </option>
                                      ))}
                                    </React.Fragment>
                                  );
                                }
                                return null;
                              }}
                            </Countries>
                          </Select>
                        </FormControl>
                        <FormControl
                          size="small"
                          variant="outlined"
                          style={{
                            width: 100
                          }}
                          className={classes.formControl}
                        >
                          <Select
                            native
                            size="small"
                            value={provinceId}
                            onChange={e => {
                              this.setState({ provinceId: e.target.value });
                            }}
                            inputProps={{
                              name: "province",
                              id: "outlined-province-native-simple"
                            }}
                          >
                            <option value="">province</option>
                            <Provinces id={countryId}>
                              {({ data }) => {
                                if (data) {
                                  return (
                                    <React.Fragment>
                                      {data.getProvincesByCountry.map(prov => (
                                        <option key={prov.id} value={prov.id}>
                                          {prov.name}
                                        </option>
                                      ))}
                                    </React.Fragment>
                                  );
                                }
                                return null;
                              }}
                            </Provinces>
                          </Select>
                        </FormControl>
                      </Container>
                      <Container flex={1} row>
                        <FormControl
                          size="small"
                          variant="outlined"
                          style={{
                            width: 100,
                            marginRight: 10,
                            marginLeft: 10
                          }}
                          className={classes.formControl}
                        >
                          <Select
                            native
                            size="small"
                            value={districtId}
                            onChange={e => {
                              this.setState({ districtId: e.target.value });
                            }}
                            inputProps={{
                              name: "district",
                              id: "outlined-district-native-simple"
                            }}
                          >
                            <option value="">district</option>
                            <Districts id={provinceId}>
                              {({ data }) => {
                                if (data) {
                                  return (
                                    <React.Fragment>
                                      {data.getDistrictsByProvince.map(dist => (
                                        <option key={dist.id} value={dist.id}>
                                          {dist.name}
                                        </option>
                                      ))}
                                    </React.Fragment>
                                  );
                                }
                                return null;
                              }}
                            </Districts>
                          </Select>
                        </FormControl>
                        <FormControl
                          size="small"
                          variant="outlined"
                          style={{
                            width: 100
                          }}
                          className={classes.formControl}
                        >
                          {/*<Select
                            native
                            size="small"
                            value={sectorId}
                            onChange={e => {
                              this.setState({ sectorId: e.target.value });
                            }}
                            inputProps={{
                              name: "sector",
                              id: "outlined-sector-native-simple"
                            }}
                          >
                            <option value="">sector</option>
                            <Sectors id={districtId}>
                              {({ data }) => {
                                if (data) {
                                  return (
                                    <React.Fragment>
                                      {data.getSectorsByDistrict.map(sect => (
                                        <option key={sect.id} value={sect.id}>
                                          {sect.name}
                                        </option>
                                      ))}
                                    </React.Fragment>
                                  );
                                }
                                return null;
                              }}
                            </Sectors>
                            </Select>*/}
                        </FormControl>
                      </Container>
                    </Container>
                  </Container>
                </Container>

                {/* action footer */}
                <Container
                  right
                  center
                  row
                  flex={0}
                  space="space-between"
                  customStyles={{
                    // position: "absolute",
                    // bottom: 0,
                    marginTop: 15,
                    padding: 10,
                    borderTop: `0.5px solid ${fade("#393939", 0.2)}`,
                    width: "100%"
                  }}
                >
                  {!success && (
                    <PrimaryButton type="submit">
                      {loading ? (
                        <img
                          src={loadingImg}
                          style={{ width: 20, height: 20 }}
                        />
                      ) : (
                        "Save"
                      )}
                    </PrimaryButton>
                  )}
                  {success && (
                    <PrimaryButton style={{ backgroundColor: "green" }}>
                      Saved
                    </PrimaryButton>
                  )}
                </Container>
              </form>
            </Container>
          );
        }}
      </Mutation>
    );
  }
}

export default withStyles(styles)(CreateHouse);
