import React from "react";
import clsx from "clsx";
import Link from "next/link";
import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import DashboardIcon from "@material-ui/icons/Dashboard";
import TenantsIcon from "@material-ui/icons/People";
import HouseIcon from "@material-ui/icons/Home";
import TicketsIcon from "@material-ui/icons/Mail";
import PersonIcon from "@material-ui/icons/Person";
import MenuCloseIcon from "@material-ui/icons/MenuOpen";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { theme as customTheme } from "../Page";
import CustomLink from "../CustomLink";
import { Container, SubText } from "../styledComponents";

const drawerWidth = 200;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },

  menuButton: {
    marginRight: 36
  },
  hide: {
    display: "none"
  },

  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    border: "none",
    backgroundColor: customTheme.bgSecondary,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    border: "none",
    backgroundColor: customTheme.bgSecondary,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  },
  content: {
    // flexGrow: 1,
    // width: "100%",
    backgroundColor: customTheme.bgPrimary
    // paddingTop: 64
    // paddingLeft: theme.spacing(20)
    // paddingRight: theme.spacing(2)
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  },
  linkStyle: {
    textDecoration: "none",
    // cursor: "pointer",
    color: customTheme.iconStyle
  },
  activeLink: {
    textDecoration: "none",
    // cursor: "pointer",
    color: customTheme.tealPrimary
  }
}));

const NavText = styled(SubText)`
  color: ${({ theme }) => theme.textPrimary};
  font-weight: 500;
  &:hover {
    color: ${({ theme }) => theme.cyanPrimary};
  }
`;

const NavItem = styled(Container)`
  display: flex;
  flex-direction: row;
  align-items: center;
  /* border-radius: 100px; */
  padding: 12px;
  background-color: transparent;
  cursor: pointer;
  &:hover {
    background-color: ${({ theme }) => theme.bgPrimary};
  }
`;
const WithLayout = Page => props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />

      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
      >
        <div className={classes.toolbar}>
          <div style={{ marginLeft: 2 }}>
            <IconButton onClick={handleDrawerOpen}>
              {open ? (
                <MenuCloseIcon style={{ fontSize: 22, color: "black" }} />
              ) : (
                <MenuIcon style={{ fontSize: 20, color: "black" }} />
              )}
            </IconButton>
          </div>
        </div>

        <Container
          flex={1}
          column
          color="transparent"
          customStyles={{ height: "100%", paddingLeft: 10, paddingRight: 10 }}
        >
          <Link
            href={"/landlord/dashboard"}
            // href={{
            //   pathname: "/landlord/dashboard",
            //   query: { title: "Dashboard" }
            // }}
            as={"/landlord/dashboard"}
            className={classes.linkStyle}
            activeClassName={classes.activeLink}
          >
            <a>
              {!open && (
                <IconButton>
                  <DashboardIcon style={{ fontSize: 20, color: "#717D7E" }} />
                </IconButton>
              )}

              {open && (
                <NavItem>
                  <Container row left center flex={0} color="transparent">
                    <DashboardIcon style={{ fontSize: 20, color: "#717D7E" }} />
                  </Container>

                  <Container
                    row
                    left
                    center
                    flex={1}
                    color="transparent"
                    customStyles={{ marginLeft: 20 }}
                  >
                    <NavText>Dashboard</NavText>
                  </Container>
                </NavItem>
              )}
            </a>
          </Link>
          <Link
            href={{ pathname: "/landlord/houses", query: { title: "Houses" } }}
            as={"/landlord/houses"}
          >
            <a>
              {!open && (
                <IconButton>
                  <HouseIcon style={{ fontSize: 20, color: "#717D7E" }} />
                </IconButton>
              )}

              {open && (
                <NavItem>
                  <Container row left center flex={0} color="transparent">
                    <HouseIcon style={{ fontSize: 20, color: "#717D7E" }} />
                  </Container>

                  <Container
                    row
                    left
                    center
                    flex={1}
                    color="transparent"
                    customStyles={{ marginLeft: 20 }}
                  >
                    <NavText>House</NavText>
                  </Container>
                </NavItem>
              )}
            </a>
          </Link>
          <Link as={"/landlord/tenants"} href={"/landlord/tenants"}>
            <a>
              {!open && (
                <IconButton>
                  <TenantsIcon style={{ fontSize: 20, color: "#717D7E" }} />
                </IconButton>
              )}

              {open && (
                <NavItem>
                  <Container row left center flex={0} color="transparent">
                    <TenantsIcon style={{ fontSize: 20, color: "#717D7E" }} />
                  </Container>

                  <Container
                    row
                    left
                    center
                    flex={1}
                    color="transparent"
                    customStyles={{ marginLeft: 20 }}
                  >
                    <NavText>Tenants</NavText>
                  </Container>
                </NavItem>
              )}
            </a>
          </Link>
          <Link as={"/landlord/tickets"} href={"/landlord/tickets"}>
            <a>
              {!open && (
                <IconButton>
                  <TicketsIcon style={{ fontSize: 20, color: "#717D7E" }} />
                </IconButton>
              )}

              {open && (
                <NavItem>
                  <Container row left center flex={0} color="transparent">
                    <TicketsIcon style={{ fontSize: 20, color: "#717D7E" }} />
                  </Container>

                  <Container
                    row
                    left
                    center
                    flex={1}
                    color="transparent"
                    customStyles={{ marginLeft: 20 }}
                  >
                    <NavText>Requests</NavText>
                  </Container>
                </NavItem>
              )}
            </a>
          </Link>
        </Container>
      </Drawer>

      <Container
        flex={1}
        column
        customStyles={{
          paddingTop: 64,
          // paddingLeft: 10,
          paddingRight: 10,
          minHeight: "99.9vh"
        }}
      >
        <Page open={open} />
      </Container>
    </div>
  );
};

export default WithLayout;
