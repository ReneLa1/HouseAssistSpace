import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { SubText, CaptionText } from "./Text";
import Container from "./Container";

const useStyles = makeStyles(theme => ({
  inputStyle: {
    border: 0,
    borderRadius: 4,
    padding: 10,
    backgroundColor: "#F5F8FA",
    // lineHeight: 2,
    fontSize: 12,
    "&:focus": {
      border: "1px solid #4C3AF7",
      backgroundColor: "#FEFEFF"
    }
  }
}));

const Input = ({ label, ...other }) => {
  const classes = useStyles();
  return (
    <Container column customStyles={{ marginBottom: 10 }} color="transparent">
      <CaptionText customStyles={{ marginBottom: 5 }}>{label}</CaptionText>
      <input className={classes.inputStyle} {...other} />
    </Container>
  );
};

export default Input;
