import React, { Component } from "react";
import Meta from "./Meta";
import styled, { ThemeProvider, createGlobalStyle } from "styled-components";
// import { theme } from "../theme/Theme";
import { StylesProvider } from "@material-ui/styles";
import Container from "./styledComponents/Container";
import Router from "next/router";
import NProgress from "nprogress";

Router.onRouteChangeStart = () => {
  NProgress.start();
};
Router.onRouteChangeComplete = () => {
  NProgress.done();
};

Router.onRouteChangeError = () => {
  NProgress.done();
};
export const theme = {
  greenDarkAlt: "#63b814",
  greenDark: "#549b11",
  greenPrimary: "#6FCC16",
  greenSecondary: "#7dd22e",
  greenTertiary: "#a2e069",
  greenLight: "#cff0b0",
  greenLighter: "#e5f7d4",

  tealDarkAlt: "#00877a",
  tealDark: "#007267",
  tealPrimary: "#009688",
  tealSecondary: "#14a395",
  tealTertiary: "#4dc0b5",
  tealLight: "#9de0d9",
  tealLighter: "#c8eeea",

  cyanDarkAlt: "#00a9b5",
  cyanDark: "#008f99",
  cyanPrimary: "#00BCCA",
  cyanSecondary: "#19c4d0",
  cyanTertiary: "#59d6df",
  cyanLight: "#a7eaef",
  cyanLighter: "#cff4f6",

  blueDarkAlt: "#5279b3",
  blueDark: "#466697",
  bluePrimary: "#5B86C6",
  blueSecondary: "#6c93ce",
  blueTertiary: "#96b2dd",
  blueLight: "#c8d7ee",
  blueLighter: "#e1e9f6",

  redDarkAlt: "#d55858",
  redDark: "#b44a4a",
  redPrimary: "#ED6161",
  redSecondary: "#ef7373",
  redTertiary: "#f49e9e",
  redLight: "#facdcd",
  redLighter: "#fce4e4",

  orangeDarkAlt: "#ae785b",
  orangeDark: "#93654d",
  orangePrimary: "#C18564",
  orangeSecondary: "#c99274",
  orangeTertiary: "#dab19b",
  orangeLight: "#edd6cb",
  orangeLighter: "#f5e9e2",

  deepBlueDarkAlt: "#4433df",
  deepBlueDark: "#3a2bbc",
  deepBluePrimary: "#4C3AF7",
  deepBlueSecondary: "#6150f8",
  deepBlueTertiary: "#9287fa",
  deepBlueLight: "#c8c2fd",
  deepBlueLighter: "#e2dffe",

  // bgPrimary: "#F5F8FA",
  // bgPrimary: "#EAECEE",
  bgPrimary: "#F2F3F4",
  bgSecondary: "#FEFEFF",
  btnPrimary: "#0078D4",
  textPrimary: "#2A3045",
  // textPrimary: "#323130",
  // textPrimary: "#0D0D0C",
  textSecondary: "#50566B",
  iconStyle: "#717D7E",

  blue: "#6C8EEF",
  red: "#FF0000",
  black: "#393939",
  grey: "#3A3A3A",
  grayHover: "#F3F2F1",
  lightGrey: "#E1E1E1",
  offWhite: "#EDEDED",
  maxWidth: "1000px",
  bs: "0 12px 24px 0 rgba(0, 0, 0, 0.09)"
};

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Noto+Serif|PT+Serif+Caption|Raleway&display=swap');
// @font-face {
//   font-family: 'Noto Serif', serif;
//   font-display: swap;
//   src: url('/public/static/Noto_Serif/NotoSerif-Regular.ttf') format('ttf');
//   font-weight: normal;
//   font-style: normal;
// }
// font-family: 'Noto Serif', serif;
font-family: 'Raleway', sans-serif;
font-style: normal;
font-weight: 400;
  html {
    box-sizing: border-box;
    font-size: 10px;
    line-height:14px;
    outline:none;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  input{
    outline:none;
  }
  select{
    outline:none;
  }
  textarea{
    outline:none;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.2rem;
    line-height: 1;
    background-color:${theme.bgPrimary};
    color:${theme.textPrimary};
    // font-family: 'Noto Serif', serif;
    font-weight:400;
    font-family: 'Raleway', sans-serif;
    // font-family: 'PT Serif Caption', serif;
    outline:none;
  }
  a {
    text-decoration: none;
    color: ${theme.textPrimary};
  }
  button { 
  font-family: 'Noto Serif', serif;
  font-weight:400;
  outline:none; 
}
`;

class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Container flex={1} column>
          <Meta />
          {this.props.children}
        </Container>
      </ThemeProvider>
    );
  }
}

export default Page;

{
  /*<StylesProvider injectFirst>*/
}
{
  /*</StylesProvider>*/
}
