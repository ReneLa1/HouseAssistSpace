import Link from "next/link";
import hasLogo from "../public/static/HAS.png";
import User from "../components/queryComponents/User";
import { theme } from "../components/Page";
import LogoutButton from "../components/landlord/LogoutButton";
import {
  Container,
  PrimaryButton,
  OutlinedButton,
  CommandButton,
  ActionButton,
  IconButton,
  Title,
  SubHeader,
  Header,
  Text,
  CaptionText,
  SubText,
  CardStyles,
  CardContent,
  CardImage,
  CardFooter,
  CardActions
} from "../components/styledComponents";
import { Typography } from "@material-ui/core";

const Home = () => {
  return (
    <Container flex={1} column>
      <Container flex={0} middle center>
        <img src={hasLogo} style={{ width: "7%", height: "7%" }} />
      </Container>
      <Container flex={1} column middle center>
        <SubHeader>Home</SubHeader>
        <User>
          {({ data }) => {
            if (data) {
              const { me } = data;
              return (
                <Container row middle center>
                  <p>{me && "logged in as: " + me.user_name}</p>
                  {me && (
                    <Container column middle center>
                      <LogoutButton />
                    </Container>
                  )}
                </Container>
              );
            }
            return null;
          }}
        </User>
        <Link href={"/landlord/login"}>
          <a style={{ color: "green" }}>Landlord</a>
        </Link>
      </Container>
      <Container row flex={1}>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginLeft: 10 }}
        >
          <span>Green</span>
          <Container column space="space-around">
            <Container color={theme.greenDarkAlt} middle center>
              greenDarkAlt
            </Container>
            <Container color={theme.greenDark} middle center>
              greenDark
            </Container>
            <Container color={theme.greenPrimary} middle center>
              greenPrimary
            </Container>
            <Container color={theme.greenSecondary} middle center>
              greenSecondary
            </Container>
            <Container color={theme.greenTertiary} middle center>
              greenTertiary
            </Container>
            <Container color={theme.greenLight} middle center>
              greenLight
            </Container>
            <Container color={theme.greenLighter} middle center>
              greenLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span>Teal</span>
          <Container column space="space-around">
            <Container color={theme.tealDarkAlt} middle center>
              tealDarkAlt
            </Container>
            <Container color={theme.tealDark} middle center>
              tealDark
            </Container>
            <Container color={theme.tealPrimary} middle center>
              tealPrimary
            </Container>
            <Container color={theme.tealSecondary} middle center>
              tealSecondary
            </Container>
            <Container color={theme.tealTertiary} middle center>
              tealTertiary
            </Container>
            <Container color={theme.tealLight} middle center>
              tealLight
            </Container>
            <Container color={theme.tealLighter} middle center>
              tealLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span>cyan</span>
          <Container column space="space-around">
            <Container color={theme.cyanDarkAlt} middle center>
              cyanDarkAlt
            </Container>
            <Container color={theme.cyanDark} middle center>
              cyanDark
            </Container>
            <Container color={theme.cyanPrimary} middle center>
              cyanPrimary
            </Container>
            <Container color={theme.cyanSecondary} middle center>
              cyanSecondary
            </Container>
            <Container color={theme.cyanTertiary} middle center>
              cyanTertiary
            </Container>
            <Container color={theme.cyanLight} middle center>
              cyanLight
            </Container>
            <Container color={theme.cyanLighter} middle center>
              cyanLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span>blue</span>
          <Container column space="space-around">
            <Container color={theme.blueDarkAlt} middle center>
              blueDarkAlt
            </Container>
            <Container color={theme.blueDark} middle center>
              blueDark
            </Container>
            <Container color={theme.bluePrimary} middle center>
              bluePrimary
            </Container>
            <Container color={theme.blueSecondary} middle center>
              blueSecondary
            </Container>
            <Container color={theme.blueTertiary} middle center>
              blueTertiary
            </Container>
            <Container color={theme.blueLight} middle center>
              blueLight
            </Container>
            <Container color={theme.blueLighter} middle center>
              blueLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span>red</span>
          <Container column space="space-around">
            <Container color={theme.redDarkAlt} middle center>
              redDarkAlt
            </Container>
            <Container color={theme.redDark} middle center>
              redDark
            </Container>
            <Container color={theme.redPrimary} middle center>
              redPrimary
            </Container>
            <Container color={theme.redSecondary} middle center>
              redSecondary
            </Container>
            <Container color={theme.redTertiary} middle center>
              redTertiary
            </Container>
            <Container color={theme.redLight} middle center>
              redLight
            </Container>
            <Container color={theme.redLighter} middle center>
              redLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span>orange</span>
          <Container column space="space-around">
            <Container color={theme.orangeDarkAlt} middle center>
              orangeDarkAlt
            </Container>
            <Container color={theme.orangeDark} middle center>
              orangeDark
            </Container>
            <Container color={theme.orangePrimary} middle center>
              orangePrimary
            </Container>
            <Container color={theme.orangeSecondary} middle center>
              orangeSecondary
            </Container>
            <Container color={theme.orangeTertiary} middle center>
              orangeTertiary
            </Container>
            <Container color={theme.orangeLight} middle center>
              orangeLight
            </Container>
            <Container color={theme.orangeLighter} middle center>
              orangeLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span style={{ color: theme.textSecondary, fontSize: 15 }}>
            deepBlue
          </span>
          <Container column space="space-around">
            <Container color={theme.deepBlueDarkAlt} middle center>
              deepBlueDarkAlt
            </Container>
            <Container color={theme.deepBlueDark} middle center>
              deepBlueDark
            </Container>
            <Container color={theme.deepBluePrimary} middle center>
              deepBluePrimary
            </Container>
            <Container color={theme.deepBlueSecondary} middle center>
              deepBlueSecondary
            </Container>
            <Container color={theme.deepBlueTertiary} middle center>
              deepBlueTertiary
            </Container>
            <Container color={theme.deepBlueLight} middle center>
              deepBlueLight
            </Container>
            <Container color={theme.deepBlueLighter} middle center>
              deepBlueLighter
            </Container>
          </Container>
        </Container>
        <Container
          column
          flex={1}
          middle
          center
          customStyles={{ marginBottom: 10 }}
        >
          <span style={{ color: theme.textPrimary, fontSize: 15 }}>more</span>
          <Container column space="space-around">
            <Container color={theme.bgPrimary} middle center>
              bgPrimary
            </Container>
            <Container color={theme.bgSecondary} middle center>
              bgSecondary
            </Container>
            <Container color={theme.textPrimary} middle center>
              textPrimary
            </Container>
            <Container color={theme.textSecondary} middle center>
              textSecondary
            </Container>
            <Container color={theme.iconStyle} middle center>
              iconStyle
            </Container>
            <Container color={theme.blue} middle center>
              blue
            </Container>
            <Container color={theme.red} middle center>
              red
            </Container>
            <Container color={theme.black} middle center>
              black
            </Container>
            <Container color={theme.grey} middle center>
              grey
            </Container>
          </Container>
        </Container>
      </Container>
      <Container flex={0} middle center row>
        <PrimaryButton
          onClick={() => {
            alert("here i work");
          }}
        >
          Primary
        </PrimaryButton>
        <OutlinedButton
          customStyles={{ marginLeft: 10 }}
          onClick={() => {
            alert("here i work");
          }}
        >
          Outline
        </OutlinedButton>
        <CommandButton
          customStyles={{
            marginLeft: 10
          }}
        >
          <span
            style={{
              marginRight: 5,
              // marginLeft: 3,
              padding: 0,
              fontWeight: 500,
              fontSize: "24px"
            }}
          >
            +
          </span>
          New House
        </CommandButton>
        <ActionButton>create account</ActionButton>
        <IconButton>
          <span>&times;</span>
        </IconButton>
      </Container>
      <Container
        row
        space="space-evenly"
        flex={1}
        customStyles={{ padding: 15 }}
      >
        <Container
          middle
          flex={1}
          center
          color="white"
          depth={4}
          customStyles={{ minHeight: 100, width: 70, margin: 5 }}
        >
          <span>Cards</span>
          <span>Grid item thumbnails</span>
        </Container>
        <Container
          middle
          center
          column
          flex={1}
          depth={8}
          customStyles={{ minHeight: 100, width: 70, margin: 5 }}
        >
          <span>Command bars</span>
          <span>Command dropdowns</span>
          <span>Context menus</span>
        </Container>
        <Container
          middle
          center
          depth={16}
          flex={1}
          customStyles={{ minHeight: 100, width: 70, margin: 5 }}
        >
          <span>Teaching call outs</span>
          <span>Search results drop-downs</span>
          <span>hover cards</span>
          <span>tooltip</span>
        </Container>
        <Container
          middle
          center
          depth={64}
          flex={1}
          customStyles={{ minHeight: 100, width: 70, margin: 5 }}
        >
          <span>pop up dialogs</span>
          <span>panels</span>
        </Container>
      </Container>
      <Container flex={1} row>
        <CardStyles column depth={4}>
          <CardActions flex={0.1}>
            <ActionButton>edit</ActionButton>
            <ActionButton>close</ActionButton>
          </CardActions>
          <CardFooter flex={0.1}>
            <span>edit</span>
            <span>close</span>
            <span>close</span>
            <span>edit</span>
            <span>close</span>
            <span>close</span>
          </CardFooter>
        </CardStyles>
      </Container>

      <Container column>
        <Title>Dialog</Title>
        <Header>Overview</Header>
        <SubHeader>Don't</SubHeader>
        <Text>
          Dialogs are temporary, modal UI overlay that generally provide
          contextual app information or require user confirmation/input. In most
          cases, Dialogs block interactions with the web page or application
          until being explicitly dismissed, and often request action from the
          user. They are primarily used for lightweight creation or edit tasks,
          and simple management tasks.
        </Text>
        <SubText>
          Dialogs block interactions with the web page or application until
          being explicitly dismissed, and often request action from the user.
          They are primarily used for lightweight creation or edit tasks, and
          simple management tasks.
        </SubText>
        <CaptionText>
          And often request action from the user. They are primarily used for
          lightweight creation or edit tasks, and simple management tasks.
        </CaptionText>
      </Container>
    </Container>
  );
};
export default Home;
