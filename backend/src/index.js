const cookieParser = require("cookie-parser");
require("dotenv").config({ path: "variables.env" });
const createServer = require("./createServer");
const jwt = require("jsonwebtoken");
const db = require("./db");

const server = createServer();

//middleware to handle cookies (JWT)
server.express.use(cookieParser());

//Decode the JWT so we can get the user ID on each request
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (token) {
    const { userId } = jwt.verify(token, process.env.APP_SECRET);

    //put the userId onto the req for future request to access
    req.userId = userId;
  }
  next();
});

// middleware to populate current user
server.express.use(async (req, res, next) => {
  //if not logged in skip
  if (!req.userId) return next();
  const user = await db.query.user(
    { where: { id: req.userId } },
    "{id,userType, permissions,email}"
  );
  req.user = user;
  next();
});

server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    }
  },
  deets => {
    console.log(`Server is now running on port http://localhost:${deets.port}`);
  }
);
