const { forwardTo } = require("prisma-binding");

const validateUser = (own, type, permit) => {
  if (!own) {
    throw new Error("you dont own the building");
  } else if (!permit) {
    throw new Error("do not have permission");
  } else if (!type) {
    throw new Error("you are no the user");
  }
};

const isLogged = request => {
  if (!request.userId) {
    throw new Error("login first");
  }
};

const Query = {
  me(parent, args, ctx, info) {
    //check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }
    return ctx.db.query.user(
      {
        where: { id: ctx.request.userId }
      },
      info
    );
  },

  // get users
  users: forwardTo("db"),
  user: forwardTo("db"),

  // landlord related query resolvers
  landlords: forwardTo("db"),
  landlord: forwardTo("db"),
  async currentLandlord(parent, args, ctx, info) {
    //check if user exists
    if (!ctx.request.userId) {
      throw new Error("login first");
    }
    //get landlord with id

    const current_landlord = await ctx.db.query.landlords(
      {
        where: {
          userId: {
            id: ctx.request.userId
          }
        }
      },
      info
    );

    return current_landlord;
  },

  // tenant related query resolvers
  tenants: forwardTo("db"),
  tenant: forwardTo("db"),
  async getTenant(parent, args, ctx, info) {
    //check if logged in

    //get tenant
    const tenant = await ctx.db.query.tenant({
      where: {
        id: args.id
      }
    });

    return tenant;
  },
  async getUserTenants(parent, args, ctx, info) {
    //check if logged in

    //get tenants
    const tenants = await ctx.db.query.tenants(
      {
        where: {
          registeredBy: {
            id: ctx.request.userId
          }
        }
      },
      info
    );

    return tenants;
  },
  // agent related query resolvers
  agents: forwardTo("db"),
  agent: forwardTo("db"),

  // agent group related query resolvers
  agentGroups: forwardTo("db"),
  agentGroup: forwardTo("db"),

  // agent group coverage related query resolvers
  agentCoverages: forwardTo("db"),
  agentCoverage: forwardTo("db"),

  // house types related query resolvers
  houseTypes: forwardTo("db"),
  houseType: forwardTo("db"),

  // parent house related query resolvers
  parentHouses: forwardTo("db"),
  parentHouse: forwardTo("db"),
  async getUserParentHouses(parent, args, ctx, info) {
    //1. check if he is logged in
    //2. get parent houses created by user
    const buildings = await ctx.db.query.parentHouses(
      {
        where: { owner: { id: ctx.request.userId } }
      },
      info
    );

    return buildings;
  },

  // parent house image related query resolvers
  parentHouseImages: forwardTo("db"),
  parentHouseImage: forwardTo("db"),

  // house related query resolvers
  async getUserHouses(_, args, ctx, info) {
    //check if there is a current user id
    // if (!ctx.request.userId) {
    //   return null;
    // }
    const houses = await ctx.db.query.houses(
      {
        where: {
          registeredBy: {
            id: ctx.request.userId
          }
        }
      },
      info
    );
    return houses;
  },
  async getHouse(parent, args, ctx, info) {
    //1. check if is logged in
    // isLogged(ctx.request);

    const house = await ctx.db.query.house(
      {
        where: {
          id: args.id
        }
      },
      info
    );
    return house;
  },
  houses: forwardTo("db"),
  house: forwardTo("db"),

  // house image related query resolvers
  async getHouseImages(parent, args, ctx, info) {
    const images = await ctx.db.query.houseImages(
      {
        where: {
          houseId: { id: args.id }
        }
      },
      info
    );
    return images;
  },
  houseImages: forwardTo("db"),
  houseImage: forwardTo("db"),

  // leasing info related query resolvers
  leaseInfoes: forwardTo("db"),
  leaseInfo: forwardTo("db"),
  async getLeases(parent, args, ctx, info) {
    const leases = await ctx.db.query.leaseInfoes(
      {
        where: {
          owner: {
            id: ctx.request.userId
          }
        }
      },
      info
    );

    return leases;
  },
  // subscription plan related query resolvers
  subscriptionPlans: forwardTo("db"),
  subscriptionPlan: forwardTo("db"),

  // subscriber related query resolvers
  subscribers: forwardTo("db"),
  subscriber: forwardTo("db"),

  // ticket related query resolvers
  tickets: forwardTo("db"),
  ticket: forwardTo("db"),

  // country related query resolvers
  countries: forwardTo("db"),
  country: forwardTo("db"),

  // province related query resolvers
  provinces: forwardTo("db"),
  async getProvincesByCountry(_, args, ctx, info) {
    const provinces = await ctx.db.query.provinces(
      {
        where: {
          countryId: {
            id: args.id
          }
        }
      },
      info
    );
    return provinces;
  },
  province: forwardTo("db"),

  // district related query resolvers
  districts: forwardTo("db"),
  async getDistrictsByProvince(_, args, ctx, info) {
    const districts = await ctx.db.query.districts(
      {
        where: {
          provinceId: {
            id: args.id
          }
        }
      },
      info
    );
    return districts;
  },
  district: forwardTo("db"),

  // sector related query resolvers
  // sectors: forwardTo("db"),
  async getSectorsByDistrict(_, args, ctx, info) {
    const sectors = await ctx.db.query.sectors(
      {
        where: {
          districtId: {
            id: args.id
          }
        }
      },
      info
    );
    return sectors;
  },
  sector: forwardTo("db")
};

module.exports = Query;
