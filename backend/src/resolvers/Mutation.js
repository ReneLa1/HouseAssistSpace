const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const validateUser = (own, type, permit) => {
  if (!own) {
    throw new Error("you dont own the building");
  } else if (!permit) {
    throw new Error("do not have permission");
  } else if (!type) {
    throw new Error("you are no the user");
  }
};

const isLogged = request => {
  if (!request.userId) {
    throw new Error("login first");
  }
};
const Mutations = {
  // user resolvers
  async signup(parent, args, ctx, info) {
    //lowercase email
    args.email = args.email.toLowerCase();

    // hash password
    const password = await bcrypt.hash(args.password, 10);

    // create user
    const user = await ctx.db.mutation.createUser(
      {
        data: {
          ...args,
          password,
          userType: { set: ["USER"] }
        }
      },
      info
    );

    // create jwt token
    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);

    // we set a cookie on the response
    ctx.response.cookie("token", token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365 // 1 year cookie
    });
    return user;
  },

  async login(parent, { email, password }, ctx, info) {
    //1. check if email exists
    const user = await ctx.db.query.user({ where: { email } });
    if (!user) {
      throw new Error("No such user found");
    }

    //2. check if their password is correct
    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error("Invalid password");
    }
    //3. generate the JWT token
    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
    //4. set the cookie with the token
    ctx.response.cookie("token", token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365 // 1 year cookie
    });
    //5. return the user
    return user;
  },

  async logout(parent, args, ctx, info) {
    await ctx.response.clearCookie("token");
    return { message: "logged out" };
  },

  // landlord  resolvers
  async createLandlord(parent, args, ctx, info) {
    const { userId } = ctx.request;
    //check if is logged in and valid user
    if (!userId) {
      throw new Error("log in first");
    }

    //check if already exist this landlord
    const [existingLandlord] = await ctx.db.query.landlords({
      where: {
        userId: { id: userId }
      }
    });
    if (existingLandlord) {
      throw new Error("landlord  exists");
      // console.log(existingLandlord);
      // return;
    }

    await ctx.db.mutation.createLandlord(
      {
        data: {
          ...args,
          userId: {
            connect: {
              id: userId
            }
          }
          // countryId: {
          //   connect: {
          //     id: args.countryId
          //   }
          // },
          // provinceId: {
          //   connect: {
          //     id: args.provinceId
          //   }
          // },
          // districtId: {
          //   connect: {
          //     id: args.districtId
          //   }
          // },

          // sectorId: {
          //   connect: {
          //     id: args.sectorId
          //   }
          // }
        }
      },
      info
    );

    await ctx.db.mutation.updateUser(
      {
        data: {
          userType: { set: ["LANDLORD"] },
          permissions: {
            set: [
              "CREATE_HOUSE",
              "UPDATE_HOUSE",
              "DELETE_HOUSE",
              "CREATE_TENANT",
              "UPDATE_TENANT",
              "CREATE_HOUSE_IMAGE",
              "UPDATE_HOUSE_IMAGE",
              "DELETE_HOUSE_IMAGE",
              "CREATE_PARENTHOUSE",
              "UPDATE_PARENTHOUSE",
              "DELETE_PARENTHOUSE",
              "CREATE_PARENTHOUSE_IMAGE",
              "UPDATE_PARENTHOUSE_IMAGE",
              "DELETE_PARENTHOUSE_IMAGE"
            ]
          }
        },
        where: {
          id: ctx.request.userId
        }
      },
      info
    );
    return;
  },

  updateLandlord(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    return ctx.db.mutation.updateLandlord(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteLandlord(parent, args, ctx, info) {
    const where = { id: args.id };

    const landlord = await ctx.db.query.landlord(
      { where },
      `{id,firstName, lastName}`
    );
    return await ctx.db.mutation.deleteLandlord({ where }, info);
  },

  // tenant  resolvers
  async createTenant(parent, args, ctx, info) {
    const tenant = await ctx.db.mutation.createTenant(
      {
        data: {
          ...args,
          userId: {
            connect: {
              id: args.userId
            }
          },
          registeredBy: {
            connect: {
              id: args.registeredBy
            }
          },
          houseId: {
            connect: {
              id: args.houseId
            }
          }
        }
      },
      info
    );
    return tenant;
  },
  updateTenant(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    return ctx.db.mutation.updateTenant(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteTenant(parent, args, ctx, info) {
    const where = { id: args.id };

    const tenant = await ctx.db.query.tenant({ where }, `{id}`);
    return await ctx.db.mutation.deleteTenant({ where }, info);
  },

  // agent resolvers
  async createAgent(parent, args, ctx, info) {
    const agent = await ctx.db.mutation.createAgent(
      {
        data: {
          ...args,
          agentGroup: {
            connect: {
              id: args.agentGroup
            }
          },
          userId: {
            connect: {
              id: args.userId
            }
          },
          countryId: {
            connect: {
              id: args.countryId
            }
          },
          provinceId: {
            connect: {
              id: args.provinceId
            }
          },
          districtId: {
            connect: {
              id: args.districtId
            }
          },
          sectorId: {
            connect: {
              id: args.sectorId
            }
          }
        }
      },
      info
    );
    return agent;
  },
  updateAgent(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    return ctx.db.mutation.updateAgent(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteAgent(parent, args, ctx, info) {
    const where = { id: args.id };

    const agent = await ctx.db.query.agent({ where }, `{id}`);
    return await ctx.db.mutation.deleteAgent({ where }, info);
  },

  // agent group  resolvers
  async createAgentGroup(parent, args, ctx, info) {
    const group = await ctx.db.mutation.createAgentGroup(
      {
        data: {
          ...args,
          userId: {
            connect: {
              id: args.userId
            }
          },
          countryId: {
            connect: {
              id: args.countryId
            }
          },
          provinceId: {
            connect: {
              id: args.provinceId
            }
          },
          districtId: {
            connect: {
              id: args.districtId
            }
          },
          sectorId: {
            connect: {
              id: args.sectorId
            }
          }
        }
      },
      info
    );
    return group;
  },
  updateAgentGroup(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    return ctx.db.mutation.updateAgentGroup(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteAgentGroup(parent, args, ctx, info) {
    const where = { id: args.id };

    const group = await ctx.db.query.agentGroup({ where }, `{id}`);
    return await ctx.db.mutation.deleteAgentGroup({ where }, info);
  },

  // agent coverage resolvers
  async createAgentCoverage(parent, args, ctx, info) {
    const coverage = await ctx.db.mutation.createAgentCoverage(
      {
        data: {
          ...args,
          userId: {
            connect: {
              id: args.userId
            }
          },
          agentGroup: {
            connect: {
              id: args.agentGroup
            }
          },
          provinceId: {
            connect: {
              id: args.provinceId
            }
          },
          districtId: {
            connect: {
              id: args.districtId
            }
          },
          sectorId: {
            connect: {
              id: args.sectorId
            }
          }
        }
      },
      info
    );
    return coverage;
  },
  updateAgentCoverage(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    return ctx.db.mutation.updateAgentCoverage(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteAgentCoverage(parent, args, ctx, info) {
    const where = { id: args.id };

    const coverage = await ctx.db.query.agentCoverage({ where }, `{id}`);
    return await ctx.db.mutation.deleteAgentCoverage({ where }, info);
  },

  //houseType  mutations resolvers
  async createHouseType(parent, args, ctx, info) {
    const houseType = await ctx.db.mutation.createHouseType(
      { data: { ...args } },
      info
    );
    return houseType;
  },
  updateHouseType(parent, args, ctx, info) {
    //first take a copy of updates
    const updates = { ...args };

    // remove the ID from the updates
    delete updates.id;

    //run the update method
    return ctx.db.mutation.updateHouseType(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteHouseType(parent, args, ctx, info) {
    const where = { id: args.id };

    const houseType = await ctx.db.query.houseType(
      { where },
      `{id,house_type}`
    );
    return ctx.db.mutation.deleteHouseType({ where }, info);
  },

  //parent house resolvers
  async createParentHouse(parent, args, ctx, info) {
    //1.check if is logged in

    //2. check if they own permission and role
    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD", "AGENT"].includes(type)
    );
    const hasPermissions = ctx.request.user.permissions.some(permission =>
      ["CREATE_PARENTHOUSE"].includes(permission)
    );
    if (!hasType && !hasPermissions) {
      throw new Error("you are unauthorized for this");
    }

    return await ctx.db.mutation.createParentHouse(
      {
        data: {
          ...args,
          owner: {
            connect: {
              id: ctx.request.userId
            }
          }
        }
      },
      info
    );
  },
  updateParentHouse(parent, args, ctx, info) {
    //first take a copy of updates
    const updates = { ...args };

    // remove the ID from the updates
    delete updates.id;

    //run the update method
    return ctx.db.mutation.updateParentHouse(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteParentHouse(parent, args, ctx, info) {
    const where = { id: args.id };
    //1.check if is logged in

    //2. find building first
    const building = await ctx.db.query.parentHouse(
      { where },
      `{ id building_name owner { id} }`
    );

    //3.check if the own the house, if they are landlords or own permission to delete house
    const ownBuilding = building.owner.id === ctx.request.userId;

    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD", "AGENT"].includes(type)
    );

    const hasPermissions = ctx.request.user.permissions.some(permission =>
      ["DELETE_PARENTHOUSE"].includes(permission)
    );
    validateUser(ownBuilding, hasType, hasPermissions);

    //4.delete building
    return ctx.db.mutation.deleteParentHouse({ where }, info);
  },

  // parent house image resolvers
  async createParentHouseImage(parent, args, ctx, info) {
    const parentImage = await ctx.db.mutation.createParentHouseImage(
      {
        data: {
          ...args,

          buildingId: {
            connect: {
              id: args.buildingId
            }
          }
        }
      },
      info
    );
    return parentImage;
  },
  updateParentHouseImage(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;

    return ctx.db.mutation.updateParentHouseImage(
      {
        data: { name: args.image },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteParentHouseImage(parent, args, ctx, info) {
    const where = { id: args.id };

    return await ctx.db.mutation.deleteParentHouseImage({ where }, info);
  },

  //house resolvers
  async createHouse(parent, args, ctx, info) {
    //check if is logged in
    const { userId } = ctx.request;
    if (!userId) {
      throw new Error("log in first");
    }

    //check if has role or permission to create house
    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD", "AGENT"].includes(type)
    );
    const hasPermissions = ctx.request.user.permissions.some(permission =>
      ["CREATE_HOUSE"].includes(permission)
    );
    if (!hasType && !hasPermissions) {
      throw new Error("you are unauthorized for this");
    }
    return await ctx.db.mutation.createHouse(
      {
        data: {
          ...args,
          registeredBy: {
            connect: {
              id: userId
            }
          },
          house_type: {
            connect: {
              id: args.house_type
            }
          },
          // parent_houseId: {
          //   connect: {
          //     id: args.parent_houseId
          //   }
          // },
          countryId: {
            connect: {
              id: args.countryId
            }
          },
          provinceId: {
            connect: {
              id: args.provinceId
            }
          },
          districtId: {
            connect: {
              id: args.districtId
            }
          }
          // sectorId: {
          //   connect: {
          //     id: args.sectorId
          //   }
          // }
        }
      },
      info
    );
  },
  updateHouse(parent, args, ctx, info) {
    //1. find the house

    const house = ctx.db.query.house({ where: { id: args.id } }, info);

    //check if they own, have role or permission to update house
    const ownHouse = house.registeredBy.id === ctx.request.userId;
    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD"].includes(type)
    );
    const hasPermissions = ctx.request.user.permissions.some(permission =>
      ["UPDATE_HOUSE"].includes(permission)
    );

    if (!ownHouse && !hasPermissions && hasType) {
      throw new error("You are not allowed to modify this house");
    }

    // first take a copy of the updates
    const updates = { ...args };

    //remove the ID from the updates
    delete updates.id;

    //update the house
    return ctx.db.mutation.updateHouse(
      {
        data: { updates },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteHouse(parent, args, ctx, info) {
    const where = { id: args.id };

    //1. find the house
    const house = await ctx.db.query.house(
      { where },
      `{ id house_heading registeredBy { id} }`
    );

    //2.check if the own the house, if they are landlords or own permission to delete house
    const ownHouse = house.registeredBy.id === ctx.request.userId;

    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD", "AGENT"].includes(type)
    );

    const hasPermissions = ctx.request.user.permissions.some(permission =>
      ["DELETE_HOUSE"].includes(permission)
    );

    validateUser(ownHouse, hasType, hasPermissions);
    // //3.delete house
    return ctx.db.mutation.deleteHouse({ where }, info);
  },

  //house image resolvers
  async createHouseImage(parent, args, ctx, info) {
    const image = await ctx.db.mutation.createHouseImage(
      {
        data: {
          ...args,

          houseId: {
            connect: {
              id: args.houseId
            }
          }
        }
      },
      info
    );
    return image;
  },
  updateHouseImage(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;

    return ctx.db.mutation.updateHouseImage(
      {
        data: { name: args.image },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteHouseImage(parent, args, ctx, info) {
    const where = { id: args.id };

    return await ctx.db.mutation.deleteHouseImage({ where }, info);
  },

  //lease info resolvers
  async createLeaseInfo(parent, args, ctx, info) {
    const { userId } = ctx.request;

    //check if has role or permission to create house
    const hasType = ctx.request.user.userType.some(type =>
      ["LANDLORD", "AGENT"].includes(type)
    );

    if (!hasType) {
      throw Error("not allowed to create lease");
    }

    const leaseInfo = await ctx.db.mutation.createLeaseInfo(
      {
        data: {
          ...args,
          houseId: {
            connect: {
              id: args.houseId
            }
          },
          owner: {
            connect: {
              id: userId
            }
          }
        }
      },
      info
    );
    return leaseInfo;
  },
  updateLeaseInfo(parent, args, ctx, info) {
    //first take a copy of updates
    const updates = { ...args };

    // remove the ID from the updates
    delete updates.id;

    //run the update method
    return ctx.db.mutation.updateLeaseInfo(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteLeaseInfo(parent, args, ctx, info) {
    const where = { id: args.id };
    const leaseInfo = await ctx.db.query.leaseInfo(
      { where },
      `{id,lease_name,admin_fee,brokerage_fee,security_fee}`
    );
    return ctx.db.mutation.deleteLeaseInfo({ where }, info);
  },

  // subscription resolvers
  async createSubscriptionPlan(parent, args, ctx, info) {
    const subscriptionPlan = await ctx.db.mutation.createSubscriptionPlan(
      { data: { ...args } },
      info
    );
    return subscriptionPlan;
  },
  updateSubscriptionPlan(parent, args, ctx, info) {
    //first take a copy of updates
    const updates = { ...args };

    // remove the ID from the updates
    delete updates.id;

    //run the update method
    return ctx.db.mutation.updateSubscriptionPlan(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteSubscriptionPlan(parent, args, ctx, info) {
    const where = { id: args.id };

    const subscriptionPlan = await ctx.db.query.subscriptionPlan(
      { where },
      `{id}`
    );
    return ctx.db.mutation.deleteSubscriptionPlan({ where }, info);
  },

  // subscriber resolvers
  async createSubscriber(parent, args, ctx, info) {
    const subscriber = await ctx.db.mutation.createSubscriber(
      {
        data: {
          ...args,

          subscrId: {
            connect: {
              id: args.subscrId
            }
          },
          subscriber: {
            connect: {
              id: args.subscriber
            }
          }
        }
      },
      info
    );
    return subscriber;
  },
  updateSubscriber(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;

    return ctx.db.mutation.updateSubscriber(
      {
        data: { name: args.name },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteSubscriber(parent, args, ctx, info) {
    const where = { id: args.id };

    // const suscriber = await ctx.db.query.subscriber(
    //   { where },
    //   `id`
    // );

    return await ctx.db.mutation.deleteSubscriber({ where }, info);
  },

  // tickets resolvers
  async createTicket(parent, args, ctx, info) {
    const ticket = await ctx.db.mutation.createTicket(
      {
        data: {
          ...args,

          reported_by: {
            connect: {
              id: args.reported_by
            }
          },
          case_assigned_by: {
            connect: {
              id: args.case_assigned_by
            }
          },
          case_agent: {
            connect: {
              id: args.case_agent
            }
          },
          case_assigned_to: {
            connect: {
              id: args.case_assigned_to
            }
          }
        }
      },
      info
    );
    return ticket;
  },
  updateTicket(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;

    return ctx.db.mutation.updateTicket(
      {
        data: { updates },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteTicket(parent, args, ctx, info) {
    const where = { id: args.id };

    // const ticket = await ctx.db.query.ticket(
    //   { where },
    //   `id`
    // );

    return await ctx.db.mutation.deleteTicket({ where }, info);
  },

  //country resolvers
  async createCountry(parent, args, ctx, info) {
    const country = await ctx.db.mutation.createCountry(
      { data: { ...args } },
      info
    );
    return country;
  },
  updateCountry(parent, args, ctx, info) {
    //first take a copy of updates
    const updates = { ...args };

    // remove the ID from the updates
    delete updates.id;

    //run the update method
    return ctx.db.mutation.updateCountry(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    );
  },
  async deleteCountry(parent, args, ctx, info) {
    const where = { id: args.id };
    // 1. Find the country
    const country = await ctx.db.query.country({ where }, `{id,name}`);
    return ctx.db.mutation.deleteCountry({ where }, info);
  },

  //province resolvers
  async createProvince(parent, args, ctx, info) {
    const province = await ctx.db.mutation.createProvince(
      {
        data: {
          ...args,
          //create a relationship to between province and country
          countryId: {
            connect: {
              id: args.countryId
            }
          }
        }
      },
      info
    );
    return province;
  },
  updateProvince(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;
    // find province
    return ctx.db.mutation.updateProvince(
      {
        data: { name: args.name },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteProvince(parent, args, ctx, info) {
    const where = { id: args.id };

    // const province = await ctx.db.query.province(
    //   { where },
    //   `id, name, countryId`
    // );

    return await ctx.db.mutation.deleteProvince({ where }, info);
  },

  //district resolvers
  async createDistrict(parent, args, ctx, info) {
    const district = await ctx.db.mutation.createDistrict(
      {
        data: {
          ...args,
          //create a relationship to between province and country
          provinceId: {
            connect: {
              id: args.provinceId
            }
          }
        }
      },
      info
    );
    return district;
  },
  updateDistrict(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;
    // find province
    return ctx.db.mutation.updateDistrict(
      {
        data: { name: args.name },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteDistrict(parent, args, ctx, info) {
    const where = { id: args.id };
    // const district = await ctx.db.query.district(
    //   { where },
    //   `id,name, provinceId`
    // );
    return ctx.db.mutation.deleteDistrict({ where }, info);
  },

  //sector  resolvers
  async createSector(parent, args, ctx, info) {
    const sector = await ctx.db.mutation.createSector(
      {
        data: {
          ...args,
          //create a relationship to between district and sector
          districtId: {
            connect: {
              id: args.districtId
            }
          }
        }
      },
      info
    );
    return sector;
  },
  updateSector(parent, args, ctx, info) {
    const updates = { ...args };

    delete updates.id;
    // find sector
    return ctx.db.mutation.updateSector(
      {
        data: { name: args.name },
        where: { id: args.id }
      },
      info
    );
  },
  async deleteSector(parent, args, ctx, info) {
    const where = { id: args.id };
    // const district = await ctx.db.query.sector(
    //   { where },
    //   `id,name, sectorId`
    // );
    return ctx.db.mutation.deleteSector({ where }, info);
  }
};

module.exports = Mutations;
