function hasPermission(user, permissionsNeeded) {
  const matchedPermission = user.permissions.filter(permission =>
    permissionsNeeded.includes(permission)
  );
  if (!matchedPermission.length) {
    throw new Error("you are not permitted for this action");
  }
}

exports.hasPermission = hasPermission;
