function hasType(user, typeNeeded) {
  const matchedType = user.roles.filter(have_type =>
    typeNeeded.includes(have_type)
  );

  if (!matchedType.length) {
    throw new Error("you are not allowed to do this ");
  }
}

exports.hasType = hasType;
