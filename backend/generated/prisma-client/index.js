"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Permission",
    embedded: false
  },
  {
    name: "UserType",
    embedded: false
  },
  {
    name: "User",
    embedded: false
  },
  {
    name: "Landlord",
    embedded: false
  },
  {
    name: "Tenant",
    embedded: false
  },
  {
    name: "AgentGroup",
    embedded: false
  },
  {
    name: "AgentCoverage",
    embedded: false
  },
  {
    name: "Agent",
    embedded: false
  },
  {
    name: "HouseType",
    embedded: false
  },
  {
    name: "HouseImage",
    embedded: false
  },
  {
    name: "LeaseInfo",
    embedded: false
  },
  {
    name: "ParentHouse",
    embedded: false
  },
  {
    name: "ParentHouseImage",
    embedded: false
  },
  {
    name: "House",
    embedded: false
  },
  {
    name: "Ticket",
    embedded: false
  },
  {
    name: "SubscriptionPlan",
    embedded: false
  },
  {
    name: "Subscriber",
    embedded: false
  },
  {
    name: "Country",
    embedded: false
  },
  {
    name: "Province",
    embedded: false
  },
  {
    name: "District",
    embedded: false
  },
  {
    name: "Sector",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `${process.env["PRISMA_ENDPOINT"]}`
});
exports.prisma = new exports.Prisma();
